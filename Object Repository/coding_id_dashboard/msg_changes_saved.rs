<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>msg_changes_saved</name>
   <tag></tag>
   <elementGuidId>bdb03f3e-b720-418b-bd24-da6d176731bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID'])[2]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.swal-modal</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>18121555-eb53-4710-b7c4-ac36dd34c094</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal-modal</value>
      <webElementGuid>6ba282b7-3530-49a4-a3be-76965e46e6dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>2cb258b0-fbf5-4106-b0fe-14d51e92785a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-modal</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>1a428bcd-3b9e-4ac6-9069-6adce9b708cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
    
  Berhasil Demo User II telah di edit 

    OK

    
      
      
      
    

  </value>
      <webElementGuid>43251c7e-5e6e-4e48-9771-902299317257</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;sidebar-gone light light-sidebar theme-white&quot;]/div[@class=&quot;swal-overlay swal-overlay--show-modal&quot;]/div[@class=&quot;swal-modal&quot;]</value>
      <webElementGuid>690c85fa-311a-4632-90eb-3974254ebab2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID'])[2]/following::div[3]</value>
      <webElementGuid>1373af5a-168f-4e75-87ec-8ef0c6fcd293</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[2]/following::div[4]</value>
      <webElementGuid>55d55a06-b4ed-4e13-bc54-0423eeeb1741</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>49ae4ffd-d039-42f0-9ee5-47b55efec94e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
    
  Berhasil Demo User II telah di edit 

    OK

    
      
      
      
    

  ' or . = '
    
    

    
    
  Berhasil Demo User II telah di edit 

    OK

    
      
      
      
    

  ')]</value>
      <webElementGuid>c837e328-7cfd-4621-bb0e-c2eae8a5856d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
